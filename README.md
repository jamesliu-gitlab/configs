# Configs

This repository contains miscellaneous configuration files. Files are organised
by repository name where sensible.

## Index

- `gitlab-development-kit/`
    - Contains configuration for my local GitLab development instance.