# gitlab-development-kit

Tested on:

- macOS 12.4
- arm64
- Colima

Configuration files used to set up a local GitLab instance. This will set up GDK
with:

- A local Docker executor runner.
- A self-signed container registry.

NB: Assumes that GDK has been installed, i.e. using the [one-line
installation](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md#one-line-installation)

```command
$ ln -s "$THIS_REPO/gitlab-development-kit/gdk.yml" "$GDK_CHECKOUT/gdk.yml"
$ ln -s "$THIS_REPO/gitlab-development-kit/gitlab-runner-config.toml" "$GDK_CHECKOUT/gitlab-runner-config.toml"
```

## Setup

1. [Create a loopback interface for
   GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/local_network.md#create-loopback-interface).
2. Register the runner. Use `http://gdk.test:3000` as the host URL, and `shell`
   as the executor:

```command
$ docker run --rm -it -v "$THIS_REPO"/gitlab-development-kit:/etc/gitlab-runner gitlab/gitlab-runner register
```

3. Start the runner (GDK is supposed to start it automatically, but this doesn't
   always happen). This mounts the config file as well as the Docker socket of
   the host machine into the container, so the Runner can perform [Docker in
   Docker](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html).

```command
$ docker run --rm -d --name gitlab-runner \
  -v "$THIS_REPO"/gitlab-development-kit:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest start
```

4. Replace `runner.token` in `gdk.yml` with the token generated in
   `gitlab-runner-config.toml`. As the TOML file already exists, it may have
   created a new `[[runners]]` entry.
5. Generate a self-signed certificate for the Docker registry:

```command
$ cd "$GDK_CHECKOUT"
# This will delete the existing cert and generate a new one. This is important to do
# if the hostname of the registry was modified.
$ rm -f registry_host.{key,crt} && make trust-docker-registry
```

6. Reconfigure and restart GDK:

```command
$ gdk reconfigure && gdk restart
```

7. Validate the self-signed registry configuration:

```command
# Attempt to curl the self-signed registry using the generated cert.
$ curl --cacert "$GDK_CHECKOUT/registry_host.crt` https://gdk.test:5000/v2/_catalog

# {"repositories":[]}
```

If you don't get the expected response, try `curl -v
https://gdk.test:5000/v2/_catalog` to obtain some debugging information. If the
response indicates the server as "AirTunes" or similar, you may need to [disable
AirPlay Receiving](https://developer.apple.com/forums/thread/682332) as it
listens on port 5000.

8. Validate that a non-trivial pipeline can run:
    1. Create a new blank project on GitLab with SAST enabled. The repository
       should be bootstrapped with the below `.gitlab-ci.yml` config.
    2. Create a `go.mod` and `main.go` with a "hello world" stub.
    3. Push changes and observe that the pipeline passes. The pipeline may need
       to be manually executed with the `CI_DEPENDENCY_PROXY_SERVER` variable
       set to a blank string as the [AutoDevOps build.sh
       script](https://gitlab.com/gitlab-org/cluster-integration/auto-build-image/-/blob/144e2bc09c8a17560cb1b4c3b8d7c0bccddb1cac/src/build.sh#L29-32)
       will always attempt to connect to the Dependency Proxy.

```yaml
# .gitlab-ci.yml

stages:
- build
- test
- deploy
- review
- dast
- staging
- canary
- production
- incremental rollout 10%
- incremental rollout 25%
- incremental rollout 50%
- incremental rollout 100%
- performance
- cleanup
sast:
  stage: test
include:
- template: Auto-DevOps.gitlab-ci.yml
```